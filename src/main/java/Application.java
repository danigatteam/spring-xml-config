import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.quintopino.service.CustomerService;

public class Application {

    public static void main(String[] args) {

	ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

	// setter injection
	CustomerService service = context.getBean("customerService", CustomerService.class);
	System.out.println("Setter injection: " + service.findAll().get(0).getFirstname());

	// constructor injection
	CustomerService service2 = context.getBean("customerService2", CustomerService.class);
	System.out.println("Constructor injection: " + service2.findAll().get(0).getLastname());

	// autowire by constructor
	CustomerService service3 = context.getBean("customerService3", CustomerService.class);
	System.out.println("Autowire by constructor: " + service3.findAll().get(0).getLastname());

	// autowire by type
	CustomerService service4 = context.getBean("customerService4", CustomerService.class);
	System.out.println("Autowire by type: " + service4.findAll().get(0).getLastname());
	
	// autowire by type
	CustomerService service5 = context.getBean("customerService5", CustomerService.class);
	System.out.println("Autowire by name: " + service5.findAll().get(0).getLastname());
    }

}
