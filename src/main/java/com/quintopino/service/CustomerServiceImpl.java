package com.quintopino.service;

import java.util.List;

import com.quintopino.model.Customer;
import com.quintopino.repository.CustomerRepository;

public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    /*
     * (non-Javadoc)
     * 
     * @see com.quintopino.service.CustomerService#findAll()
     */
    @Override
    public List<Customer> findAll() {
	return customerRepository.findAll();
    }

    public void setCustomerRepository(CustomerRepository customerRepository) {
	this.customerRepository = customerRepository;
    }
}
