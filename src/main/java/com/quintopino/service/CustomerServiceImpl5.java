package com.quintopino.service;

import java.util.List;

import com.quintopino.model.Customer;
import com.quintopino.repository.AnotherRepository;
import com.quintopino.repository.CustomerRepository;

public class CustomerServiceImpl5 implements CustomerService {

    private AnotherRepository customerRepository;

    /*
     * (non-Javadoc)
     * 
     * @see com.quintopino.service.CustomerService#findAll()
     */
    @Override
    public List<Customer> findAll() {
	return customerRepository.findAll();
    }

    public void setWeirdBeanName(AnotherRepository customerRepository) {
	this.customerRepository = customerRepository;
    }
}
