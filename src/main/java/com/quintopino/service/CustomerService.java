package com.quintopino.service;

import java.util.List;

import com.quintopino.model.Customer;

public interface CustomerService {

	List<Customer> findAll();

}