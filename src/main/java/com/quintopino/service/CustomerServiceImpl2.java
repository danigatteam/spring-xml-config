package com.quintopino.service;

import java.util.List;

import com.quintopino.model.Customer;
import com.quintopino.repository.CustomerRepository;

public class CustomerServiceImpl2 implements CustomerService {

    private CustomerRepository customerRepository;

    public CustomerServiceImpl2(CustomerRepository customerRepository) {
	super();
	this.customerRepository = customerRepository;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.quintopino.service.CustomerService#findAll()
     */
    @Override
    public List<Customer> findAll() {
	return customerRepository.findAll();
    }

}
