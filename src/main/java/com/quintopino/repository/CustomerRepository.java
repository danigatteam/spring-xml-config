package com.quintopino.repository;

import java.util.List;

import com.quintopino.model.Customer;

public interface CustomerRepository {

    List<Customer> findAll();

}